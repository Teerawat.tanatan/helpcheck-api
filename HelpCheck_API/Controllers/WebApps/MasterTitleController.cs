
using HelpCheck_API.Dtos;
using HelpCheck_API.Services.MasterTitles;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace HelpCheck_API.Controllers.WebApps
{
    public class MasterTitleController : BaseController
    {
        private readonly IMasterTitleService _masterTitleService;
        public MasterTitleController(IMasterTitleService masterTitleService)
        {
            _masterTitleService = masterTitleService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTitles()
        {
            var result = await _masterTitleService.GetMasterTitlesAsync();
            if (!result.IsSuccess)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, result.Data);
            }
            return Ok(result.Data);
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateMasterTitle(AddMasterDataDto addMasterDataDto)
        {
            var result = await _masterTitleService.CreateMasterTitleAsync(addMasterDataDto);
            if (!result.IsSuccess)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, result.Data);
            }
            return Ok(result.Data);
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateMasterTitle(int id, [FromBody] EditMasterDataDto editMasterDataDto)
        {
            editMasterDataDto.ID = id;
            var result = await _masterTitleService.UpdateMasterTitleAsync(editMasterDataDto);
            if (!result.IsSuccess)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, result.Data);
            }
            return Ok(result.Data);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMasterTitle(int id)
        {
            var result = await _masterTitleService.DeleteMasterTitleAsync(id);
            if (!result.IsSuccess)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, result.Data);
            }
            return Ok(result.Data);
        }
    }
}