
using HelpCheck_API.Services.MasterTitles;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace HelpCheck_API.Controllers.MobileApps
{
    public class MasterTitleController : BaseController
    {
        private readonly IMasterTitleService _masterTitleService;
        public MasterTitleController(IMasterTitleService masterTitleService)
        {
            _masterTitleService = masterTitleService;
        }

        [HttpGet]
        public async Task<IActionResult> GetTitles()
        {
            var result = await _masterTitleService.GetMasterTitlesAsync();
            if (!result.IsSuccess)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, result.Data);
            }
            return Ok(result.Data);
        }
    }
}