﻿using HelpCheck_API.Dtos;
using HelpCheck_API.Dtos.Users;
using HelpCheck_API.Services.Authentications;
using HelpCheck_API.Services.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace HelpCheck_API.Controllers.MobileApps
{
    public class AuthController : BaseController
    {
        private readonly IAuthService _authService;
        private readonly IUserService _userService;

        public AuthController(IAuthService authService, IUserService userService)
        {
            _authService = authService;
            _userService = userService;
        }

        [HttpPost("SignIn")]
        public async Task<IActionResult> SignIn([FromBody] LoginRequestDto loginRequestDto)
        {
            var result = await _authService.SignInAsync(loginRequestDto);
            if (!result.IsSuccess)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, result.Data);
            }
            return Ok(result.Data);
        }

        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody] ForgotPasswordDto forgotPasswordDto)
        {
            var result = await _userService.ResetPasswordAsync(forgotPasswordDto);
            if (!result.IsSuccess)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, result.Data);
            }
            return Ok(result.Data);
        }

        //[HttpPost("SetPassword")]
        //public async Task<IActionResult> SetPassword([FromBody] SetPasswordDto setPasswordDto)
        //{
        //    var result = new ResultResponse();
        //    if (!result.IsSuccess)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, result.Data);
        //    }
        //    return Ok(result.Data);
        //}
    }
}
