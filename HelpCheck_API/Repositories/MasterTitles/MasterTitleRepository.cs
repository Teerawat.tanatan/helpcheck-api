﻿using HelpCheck_API.Constants;
using HelpCheck_API.Data;
using HelpCheck_API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelpCheck_API.Repositories.MasterTitles
{
    public class MasterTitleRepository : IMasterTitleRepository
    {
        private readonly ApplicationDbContext _context;

        public MasterTitleRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public MasterTitle GetMasterTitleByID(int id)
        {
            if (id == 0)
            {
                return new MasterTitle();
            }

            MasterTitle data = _context.MasterTitles.Where(w => w.ID == id)?.FirstOrDefault();

            return data;
        }

        public string GetMasterTitleNameByID(int id)
        {
            if (id == 0)
            {
                return "";
            }

            MasterTitle data = _context.MasterTitles.Where(w => w.ID == id)?.FirstOrDefault();

            return data.Name;
        }

        public async Task<List<MasterTitle>> GetMasterTitlesAsync()
        {
            return await _context.MasterTitles.ToListAsync();
        }
        
        public async Task<MasterTitle> GetMasterTitleByIDAsync(int id)
        {
            if (id == 0)
            {
                return new MasterTitle();
            }

            MasterTitle data = await _context.MasterTitles.Where(w => w.ID == id)?.FirstOrDefaultAsync();

            return data;
        }
        
        public async Task<string> CreateMasterTitleAsync(MasterTitle masterTitle)
        {
            var trans = _context.Database.BeginTransaction();
            try
            {
                await _context.MasterTitles.AddAsync(masterTitle);
                await _context.SaveChangesAsync();
                await trans.CommitAsync();
                return Constant.STATUS_SUCCESS;
            }
            catch (Exception ex)
            {
                await trans.RollbackAsync();
                return ex.Message;
            }
        }

        public async Task<string> UpdateMasterTitleAsync(MasterTitle masterTitle)
        {
            var trans = _context.Database.BeginTransaction();
            try
            {
                _context.MasterTitles.Update(masterTitle);
                await _context.SaveChangesAsync();
                await trans.CommitAsync();

                return Constant.STATUS_SUCCESS;
            }
            catch (Exception ex)
            {
                await trans.RollbackAsync();
                return ex.Message;
            }
        }

        public async Task<string> DeleteMasterTitleAsync(MasterTitle masterTitle)
        {
            var trans = _context.Database.BeginTransaction();
            try
            {
                _context.MasterTitles.Remove(masterTitle);
                await _context.SaveChangesAsync();
                await trans.CommitAsync();

                return Constant.STATUS_SUCCESS;
            }
            catch (Exception ex)
            {
                await trans.RollbackAsync();
                return ex.Message;
            }
        }
    }
}
