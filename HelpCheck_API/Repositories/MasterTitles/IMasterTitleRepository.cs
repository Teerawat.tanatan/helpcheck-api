﻿using HelpCheck_API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HelpCheck_API.Repositories.MasterTitles
{
    public interface IMasterTitleRepository
    {
        Task<List<MasterTitle>> GetMasterTitlesAsync();
        MasterTitle GetMasterTitleByID(int id);
        string GetMasterTitleNameByID(int id);
        Task<MasterTitle> GetMasterTitleByIDAsync(int id);
        Task<string> CreateMasterTitleAsync(MasterTitle masterTitle);
        Task<string> UpdateMasterTitleAsync(MasterTitle masterTitle);
        Task<string> DeleteMasterTitleAsync(MasterTitle masterTitle);
    }
}
