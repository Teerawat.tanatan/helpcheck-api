﻿using HelpCheck_API.Constants;
using HelpCheck_API.Data;
using HelpCheck_API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelpCheck_API.Repositories.Users
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<User>> GetUsersAsync() => await _context.UserEntities.ToListAsync();

        public async Task<User> GetUserInfoByUserNameAsync(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                return new User();
            }

            var users = await GetUsersAsync();
            if (users is null)
            {
                return new User();
            }
            User user = users.Where(u => u.Email == username.Trim() || u.PhoneNo == username.Replace("-", "").Trim()).FirstOrDefault();
            return user;
        }

        public async Task<bool> CheckUserExists(string username)
        {
            User user = await _context.UserEntities.Where(u => u.Email == username || u.PhoneNo == username).FirstOrDefaultAsync<User>();
            if (user is null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public async Task<string> CreateUserAsync(User user)
        {
            var trans = _context.Database.BeginTransaction();
            try
            {
                await _context.UserEntities.AddAsync(user);
                await _context.SaveChangesAsync();
                await trans.CommitAsync();
                return Constant.STATUS_SUCCESS;
            }
            catch (Exception ex)
            {
                await trans.RollbackAsync();
                return ex.Message;
            }
        }

        public async Task<string> UpdateUserAsync(User user)
        {
            var trans = _context.Database.BeginTransaction();
            try
            {
                user.UpdatedDate = DateTime.Now;
                _context.UserEntities.Update(user);
                await _context.SaveChangesAsync();
                await trans.CommitAsync();

                return Constant.STATUS_SUCCESS;
            }
            catch (Exception ex)
            {
                await trans.RollbackAsync();
                return ex.Message;
            }
        }

        public async Task<User> GetUserByUserIDAsync(string userId)
        {
            var users = await GetUsersAsync();

            if (users is null)
            {
                return new User();
            }

            User user = users.Where(u => u.UserID == userId).FirstOrDefault();
            return user;
        }

        public async Task<string> DeleteUserAsync(User user)
        {
            var trans = _context.Database.BeginTransaction();
            try
            {
                _context.UserEntities.Remove(user);
                await _context.SaveChangesAsync();
                await trans.CommitAsync();

                return Constant.STATUS_SUCCESS;
            }
            catch (Exception ex)
            {
                await trans.RollbackAsync();
                return ex.Message;
            }
        }

        public async Task<User> GetUserByAccessTokenAsync(string accessToken)
        {
            if (string.IsNullOrWhiteSpace(accessToken)) return null;

            var users = await GetUsersAsync();

            if (users is null)
            {
                return new User();
            }

            User user = users.Where(w => w.Token == accessToken)?.FirstOrDefault();
            return user;
        }
    }
}
