using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelpCheck_API.Constants;
using HelpCheck_API.Data;
using HelpCheck_API.Models;
using Microsoft.EntityFrameworkCore;

namespace HelpCheck_API.Repositories.AmedQuestionMasters
{
    public class AmedQuestionMasterRepository : IAmedQuestionMasterRepository
    {
        private readonly ApplicationDbContext _context;

        public AmedQuestionMasterRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<string> CreateAmedQuestionMasterAsync(AmedQuestionMaster data)
        {
            var trans = _context.Database.BeginTransaction();
            try
            {
                await _context.AmedQuestionMasters.AddAsync(data);
                await _context.SaveChangesAsync();
                await trans.CommitAsync();
                return Constant.STATUS_SUCCESS;
            }
            catch (Exception ex)
            {
                await trans.RollbackAsync();
                return ex.Message;
            }
        }

        public async Task<string> DeleteAmedQuestionMasterAsync(AmedQuestionMaster data)
        {
            var trans = _context.Database.BeginTransaction();
            try
            {
                _context.AmedQuestionMasters.Remove(data);
                await _context.SaveChangesAsync();
                await trans.CommitAsync();

                return Constant.STATUS_SUCCESS;
            }
            catch (Exception ex)
            {
                await trans.RollbackAsync();
                return ex.Message;
            }
        }

        public async Task<AmedQuestionMaster> GetAmedQuestionMasterByIDAsync(int id)
        {
            var datas = await GetAmedQuestionMastersAsync();

            if (datas is null || id <= 0)
            {
                return new AmedQuestionMaster();
            }

            AmedQuestionMaster data = datas.Where(w => w.QuestionID == id)?.FirstOrDefault();
            return data;
        }

        public async Task<IEnumerable<AmedQuestionMaster>> GetAmedQuestionMastersAsync()
        {
            return await _context.AmedQuestionMasters.ToListAsync();
        }

        public async Task<string> UpdateAmedQuestionMasterAsync(AmedQuestionMaster data)
        {
            var trans = _context.Database.BeginTransaction();
            try
            {
                data.UpdatedDate = DateTime.Now;
                _context.AmedQuestionMasters.Update(data);
                await _context.SaveChangesAsync();

                return Constant.STATUS_SUCCESS;
            }
            catch (Exception ex)
            {
                await trans.RollbackAsync();
                return ex.Message;
            }
        }
    }
}