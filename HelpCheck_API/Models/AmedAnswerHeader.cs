using System;
using System.ComponentModel.DataAnnotations;

namespace HelpCheck_API.Models
{
    public class AmedAnswerHeader
    {
        [Key]
        public int AnswerEntryNo { get; set; }
        public string AnswerCode { get; set; }
        public string AnswerName { get; set; }
        public DateTime? AnswerPeriod { get; set; }
        public int? AnswerMemberID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}