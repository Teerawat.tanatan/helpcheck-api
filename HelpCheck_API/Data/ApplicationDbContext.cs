using HelpCheck_API.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelpCheck_API.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>().ToTable("user");
            builder.Entity<MasterAgency>().ToTable("master_agency");
            builder.Entity<MasterJobType>().ToTable("master_job_type");
            builder.Entity<MasterWorkPlace>().ToTable("master_work_place");
            builder.Entity<MasterTitle>().ToTable("master_title");
            builder.Entity<AmedAnswerHeader>().ToTable("amed_answer_header");
            builder.Entity<AmedAnswerDetail>().ToTable("amed_answer_detail");
            builder.Entity<AmedQuestionMaster>().ToTable("amed_question_master");
            builder.Entity<AmedChoiceMaster>().ToTable("amed_choice_master");
            builder.Entity<Module>().ToTable("module");
            builder.Entity<Role>().ToTable("role");
            builder.Entity<UserRolePermission>().ToTable("user_role_permission");
        }

        public DbSet<User> UserEntities { get; set; }
        public DbSet<MasterAgency> MasterAgencies { get; set; }
        public DbSet<MasterJobType> MasterJobTypes { get; set; }
        public DbSet<MasterWorkPlace> MasterWorkPlaces { get; set; }
        public DbSet<MasterTitle> MasterTitles { get; set; }
        public DbSet<AmedAnswerHeader> AmedAnswerHeaders { get; set; }
        public DbSet<AmedAnswerDetail> AmedAnswerDetails { get; set; }
        public DbSet<AmedQuestionMaster> AmedQuestionMasters { get; set; }
        public DbSet<AmedChoiceMaster> AmedChoiceMasters { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Role> RolePermissions { get; set; }
        public DbSet<UserRolePermission> UserRolePermissions { get; set; }
    }
}
