﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelpCheck_API.Dtos.Users
{
    public class EditUserDto
    {
        internal string UserID { get; set; }
        internal string AccessToken { get; set; }
        public int? TitleID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
       public string IDCard { get; set; }
        public DateTime? BirthDate { get; set; }
        public int? Gender { get; set; }
        public string Email { get; set; }
        public int? AgencyID { get; set; }
        public int? WorkPlaceID { get; set; }
        public int? JobTypeID { get; set; }
        public string PhoneNo { get; set; }

    }
}
