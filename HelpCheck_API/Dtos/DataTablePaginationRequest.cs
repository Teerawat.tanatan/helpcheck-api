﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelpCheck_API.Dtos
{
    public class DataTablePaginationRequest
    {
        public int CurrentPage { get; set; } = 1;
        public int PageSize { get; set; } = 100;
    }
}
