﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelpCheck_API.Dtos.Users
{
    public class SetPasswordDto
    {
        public string Email { get; set; }
        public string NewPassword { get; set; }
    }
}
