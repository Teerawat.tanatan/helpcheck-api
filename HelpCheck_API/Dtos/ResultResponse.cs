﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelpCheck_API.Dtos
{
    public class ResultResponse
    {
        public bool IsSuccess { get; set; }
        public object Data { get; set; }
    }
}
