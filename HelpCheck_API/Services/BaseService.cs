﻿using HelpCheck_API.Dtos.Users;
using HelpCheck_API.Helpers;
using HelpCheck_API.Repositories.Users;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace HelpCheck_API.Services
{
    public class BaseService
    {
        private static readonly AppSettingHelper appSettingHelper = new AppSettingHelper();
        public BaseService()
        {
        }

        protected LoginResponseDto CreateTokenUser(string userToken, string userId)
        {
            string jwtKey = appSettingHelper.GetConfiguration("JwtKey");

            List<Claim> claims = new List<Claim>
                {
                    new Claim(JwtRegisteredClaimNames.Sub, userToken),
                    new Claim(JwtRegisteredClaimNames.Jti, userId)
                };

            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtKey));
            SigningCredentials credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            DateTime issues = DateTime.Now;
            DateTime expires = issues.AddDays(Convert.ToDouble(appSettingHelper.GetConfiguration("JwtExpireDays")));

            JwtSecurityToken token = new JwtSecurityToken(
                 issuer: appSettingHelper.GetConfiguration("JwtIssuer"),
                 audience: appSettingHelper.GetConfiguration("JwtAudience"),
                 claims: claims,
                 expires: expires,
                 signingCredentials: credentials
             );

            LoginResponseDto loginResponse = new LoginResponseDto()
            {
                UserID = userId,
                UserName = appSettingHelper.GetConfiguration("JwtIssuer"),
                AccessToken = new JwtSecurityTokenHandler().WriteToken(token),
                ExpiresIn = issues,
                ExpireDate = expires
            };
            return loginResponse;
        }
    }
}
