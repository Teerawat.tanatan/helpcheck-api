﻿using HelpCheck_API.Dtos;
using HelpCheck_API.Dtos.Users;
using System.Threading.Tasks;

namespace HelpCheck_API.Services.Users
{
    public interface IUserService
    {
        Task<ResultResponse> GetUsersAsync();
        Task<ResultResponse> GetUserProfileByAccessTokenAsync(string accessToken);
        Task<ResultResponse> RegisterAsync(AddUserDto addUserDto);
        Task<ResultResponse> UpdateUserInfoAsync(EditUserDto editUserDto);
        Task<ResultResponse> UpdateUserByIDAsync(EditUserDto editUserDto);
        Task<ResultResponse> DeleteUserByIDAsync(string userId);
        Task<ResultResponse> ResetPasswordAsync(ForgotPasswordDto forgotPasswordDto);
        Task<ResultResponse> UpdateUserNotActiveAsync(string userId);
    }
}
