﻿using HelpCheck_API.Constants;
using HelpCheck_API.Dtos;
using HelpCheck_API.Dtos.Users;
using HelpCheck_API.Helpers;
using HelpCheck_API.Models;
using HelpCheck_API.Repositories.MasterAgencies;
using HelpCheck_API.Repositories.MasterJobTypes;
using HelpCheck_API.Repositories.MasterTitles;
using HelpCheck_API.Repositories.MasterWorkPlaces;
using HelpCheck_API.Repositories.Users;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace HelpCheck_API.Services.Users
{
    public class UserService : IUserService
    {
        #region IRepository

        private readonly IUserRepository _userRepository;
        private readonly IMasterAgencyRepository _masterAgency;
        private readonly IMasterJobTypeRepository _masterJobType;
        private readonly IMasterWorkPlaceRepository _masterWorkPlace;
        private readonly IMasterTitleRepository _masterTitle;

        #endregion

        public UserService(
            IUserRepository userRepository,
            IMasterAgencyRepository masterAgency,
            IMasterJobTypeRepository masterJobType,
            IMasterWorkPlaceRepository masterWorkPlace,
            IMasterTitleRepository masterTitle
            )
        {
            _userRepository = userRepository;
            _masterAgency = masterAgency;
            _masterJobType = masterJobType;
            _masterWorkPlace = masterWorkPlace;
            _masterTitle = masterTitle;
        }

        public async Task<ResultResponse> RegisterAsync(AddUserDto addUserDto)
        {
            if (!string.IsNullOrWhiteSpace(addUserDto.Username) && await _userRepository.CheckUserExists(addUserDto.Username))
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = Constant.STATUS_USER_ALREADY_EXISTS
                };
            }

            try
            {
                string hashPassword = PasswordHasher.Hash(addUserDto.Password);

                var user = new User();
                user.UserID = Guid.NewGuid().ToString();
                user.TitleID = addUserDto.TitleID;
                user.FirstName = addUserDto.FirstName;
                user.LastName = addUserDto.LastName;
                user.Email = addUserDto.Email;
                user.Password = hashPassword;
                user.IDCard = addUserDto.IDCard;
                user.BirthDate = addUserDto.BirthDate;
                user.Gender = addUserDto.Gender;
                user.AgencyID = addUserDto.AgencyID;
                user.WorkPlaceID = addUserDto.WorkPlaceID;
                user.JobTypeID = addUserDto.JobTypeID;
                user.PhoneNo = addUserDto.PhoneNo;
                user.IsActive = true;
                user.CreatedDate = DateTime.Now;
                user.CreatedBy = addUserDto.Email;

                string status = await _userRepository.CreateUserAsync(user);
                if (status != Constant.STATUS_SUCCESS)
                {
                    return new ResultResponse()
                    {
                        IsSuccess = false,
                        Data = status
                    };
                }

                return new ResultResponse()
                {
                    IsSuccess = true,
                    Data = status
                };
            }
            catch (Exception ex)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = ex.Message
                };
            }
        }

        public async Task<ResultResponse> DeleteUserByIDAsync(string userId)
        {
            var user = await _userRepository.GetUserByUserIDAsync(userId);

            if (user is null)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = Constant.STATUS_DATA_NOT_FOUND
                };
            }

            try
            {
                var status = await _userRepository.DeleteUserAsync(user);
                if (status != Constant.STATUS_SUCCESS)
                {
                    return new ResultResponse()
                    {
                        IsSuccess = false,
                        Data = status
                    };
                }

                return new ResultResponse()
                {
                    IsSuccess = true,
                    Data = status
                };
            }
            catch (Exception ex)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = ex.Message
                };
            }
        }

        public async Task<ResultResponse> UpdateUserNotActiveAsync(string userId)
        {
            var user = await _userRepository.GetUserByUserIDAsync(userId);

            if (user is null)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = Constant.STATUS_DATA_NOT_FOUND
                };
            }

            try
            {
                user.IsActive = false;
                var status = await _userRepository.UpdateUserAsync(user);
                if (status != Constant.STATUS_SUCCESS)
                {
                    return new ResultResponse()
                    {
                        IsSuccess = false,
                        Data = status
                    };
                }

                return new ResultResponse()
                {
                    IsSuccess = true,
                    Data = status
                };
            }
            catch (Exception ex)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = ex.Message
                };
            }
        }

        public async Task<ResultResponse> GetUserProfileByAccessTokenAsync(string accessToken)
        {
            var user = await _userRepository.GetUserByAccessTokenAsync(accessToken);
            if (user is null)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = Constant.STATUS_DATA_NOT_FOUND
                };
            }

            var userDto = new GetUserDto
            {
                UserID = user.UserID,
                TitleID = user.TitleID,
                TitleName = _masterTitle.GetMasterTitleNameByID(user.TitleID ?? 0),
                FirstName = user.FirstName,
                LastName = user.LastName,
                FullName = _masterTitle.GetMasterTitleNameByID(user.TitleID ?? 0) + user.FirstName + " " + user.LastName,
                BirthDate = user.BirthDate,
                Gender = user.Gender,
                PhoneNo = user.PhoneNo,
                AgencyID = user.AgencyID,
                AgencyName = _masterAgency.GetMasterAgencyNameByID(user.AgencyID ?? 0),
                JobTypeID = user.JobTypeID,
                JobTypeName = _masterJobType.GetMasterJobTypeNameByID(user.JobTypeID ?? 0),
                WorkPlaceID = user.WorkPlaceID,
                WorkPlaceName = _masterWorkPlace.GetMasterWorkPlaceNameByID(user.WorkPlaceID ?? 0),
                IsActive = user.IsActive,
                CreatedBy = user.CreatedBy,
                CreatedDate = user.CreatedDate
            };

            return new ResultResponse()
            {
                IsSuccess = true,
                Data = userDto
            };
        }

        public async Task<ResultResponse> GetUsersAsync()
        {
            var users = await _userRepository.GetUsersAsync();

            var user = users.Select(s => new GetUserDto()
            {
                UserID = s.UserID,
                TitleID = s.TitleID,
                TitleName = _masterTitle.GetMasterTitleNameByID(s.TitleID ?? 0),
                FirstName = s.FirstName,
                LastName = s.LastName,
                FullName = _masterTitle.GetMasterTitleNameByID(s.TitleID ?? 0) + s.FirstName + " " + s.LastName,
                BirthDate = s.BirthDate,
                Gender = s.Gender,
                PhoneNo = s.PhoneNo,
                AgencyID = s.AgencyID,
                AgencyName = _masterAgency.GetMasterAgencyNameByID(s.AgencyID ?? 0),
                JobTypeID = s.JobTypeID,
                JobTypeName = _masterJobType.GetMasterJobTypeNameByID(s.JobTypeID ?? 0),
                WorkPlaceID = s.WorkPlaceID,
                WorkPlaceName = _masterWorkPlace.GetMasterWorkPlaceNameByID(s.WorkPlaceID ?? 0),
                IsActive = s.IsActive,
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate
            });

            return new ResultResponse()
            {
                IsSuccess = true,
                Data = user
            };
        }

        public async Task<ResultResponse> UpdateUserInfoAsync(EditUserDto editUserDto)
        {
            var user = await _userRepository.GetUserByAccessTokenAsync(editUserDto.AccessToken);

            if (user is null)
            {
                return new ResultResponse()
                {
                    IsSuccess = true,
                    Data = Constant.STATUS_DATA_NOT_FOUND
                };
            }
            try
            {
                user.TitleID = editUserDto.TitleID;
                user.FirstName = editUserDto.FirstName;
                user.LastName = editUserDto.LastName;
                user.Email = editUserDto.Email;
                user.IDCard = editUserDto.IDCard;
                user.BirthDate = editUserDto.BirthDate;
                user.Gender = editUserDto.Gender;
                user.AgencyID = editUserDto.AgencyID;
                user.WorkPlaceID = editUserDto.WorkPlaceID;
                user.JobTypeID = editUserDto.JobTypeID;
                user.PhoneNo = editUserDto.PhoneNo;
                user.UpdatedBy = editUserDto.Email;

                var status = await _userRepository.UpdateUserAsync(user);

                if (status != Constant.STATUS_SUCCESS)
                {
                    return new ResultResponse()
                    {
                        IsSuccess = false,
                        Data = status
                    };
                }

                return new ResultResponse()
                {
                    IsSuccess = true,
                    Data = status
                };
            }
            catch (Exception ex)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = ex.Message
                };
            }
        }

        public async Task<ResultResponse> UpdateUserByIDAsync(EditUserDto editUserDto)
        {
            var user = await _userRepository.GetUserByUserIDAsync(editUserDto.UserID);

            if (user is null)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = Constant.STATUS_DATA_NOT_FOUND
                };
            }

            try
            {
                user.TitleID = editUserDto.TitleID;
                user.FirstName = editUserDto.FirstName;
                user.LastName = editUserDto.LastName;
                user.IDCard = editUserDto.IDCard;
                user.BirthDate = editUserDto.BirthDate;
                user.Gender = editUserDto.Gender;
                user.Email = editUserDto.Email;
                user.AgencyID = editUserDto.AgencyID;
                user.WorkPlaceID = editUserDto.WorkPlaceID;
                user.JobTypeID = editUserDto.JobTypeID;
                user.PhoneNo = editUserDto.PhoneNo;
                user.UpdatedBy = user.Email;

                var status = await _userRepository.UpdateUserAsync(user);

                if (status != Constant.STATUS_SUCCESS)
                {
                    return new ResultResponse()
                    {
                        IsSuccess = false,
                        Data = status
                    };
                }

                return new ResultResponse()
                {
                    IsSuccess = true,
                    Data = status
                };
            }
            catch (Exception ex)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = ex.Message
                };
            }
        }

        public async Task<ResultResponse> ResetPasswordAsync(ForgotPasswordDto forgotPasswordDto)
        {
            var user = await _userRepository.GetUserInfoByUserNameAsync(forgotPasswordDto.Username);

            if (user is null)
            {
                return new ResultResponse()
                {
                    IsSuccess = true,
                    Data = Constant.STATUS_DATA_NOT_FOUND
                };
            }

            try
            {
                string hashPassword = PasswordHasher.Hash("Password");

                user.Password = hashPassword;

                var status = await _userRepository.UpdateUserAsync(user);

                if (status != Constant.STATUS_SUCCESS)
                {
                    return new ResultResponse()
                    {
                        IsSuccess = false,
                        Data = status
                    };
                }

                return new ResultResponse()
                {
                    IsSuccess = true,
                    Data = status
                };
            }
            catch (Exception ex)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = ex.Message
                };
            }
        }
    }
}
