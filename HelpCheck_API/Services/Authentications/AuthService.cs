﻿using HelpCheck_API.Constants;
using HelpCheck_API.Dtos;
using HelpCheck_API.Dtos.Users;
using HelpCheck_API.Helpers;
using HelpCheck_API.Repositories.Users;
using System;
using System.Threading.Tasks;

namespace HelpCheck_API.Services.Authentications
{
    public class AuthService : BaseService, IAuthService
    {
        private readonly IUserRepository _userRepository;
        public AuthService(IUserRepository userRepository) : base()
        {
            _userRepository = userRepository;
        }

        public async Task<ResultResponse> SignInAsync(LoginRequestDto loginRequestDto)
        {
            var user = await _userRepository.GetUserInfoByUserNameAsync(loginRequestDto.Username);
            if (user is null)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = Constant.STATUS_DATA_NOT_FOUND
                };
            }

            if (!user.IsActive && !PasswordHasher.Check(user.Password, loginRequestDto.Password).Verified)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = Constant.STATUS_PASSWORD_NOT_MATCH
                };
            }

            string accessToken = CryptoEngine.Encrypt(Guid.NewGuid().ToString());
            user.Token = accessToken;
            user.ExpireDate = DateTime.Now.AddDays(7);
            await _userRepository.UpdateUserAsync(user);

            var result = CreateTokenUser(user.Token, user.UserID);

            return new ResultResponse()
            {
                IsSuccess = true,
                Data = result
            };
        }
    }
}
