using System.Threading.Tasks;
using HelpCheck_API.Dtos;

namespace HelpCheck_API.Services.MasterTitles
{
    public interface IMasterTitleService
    {
        Task<ResultResponse> GetMasterTitlesAsync();
        Task<ResultResponse> CreateMasterTitleAsync(AddMasterDataDto addMasterDataDto);
        Task<ResultResponse> UpdateMasterTitleAsync(EditMasterDataDto editMasterDataDto);
        Task<ResultResponse> DeleteMasterTitleAsync(int id);
    }
}