using System;
using System.Linq;
using System.Threading.Tasks;
using HelpCheck_API.Constants;
using HelpCheck_API.Dtos;
using HelpCheck_API.Models;
using HelpCheck_API.Repositories.MasterTitles;

namespace HelpCheck_API.Services.MasterTitles
{
    public class MasterTitleService : IMasterTitleService
    {
        private readonly IMasterTitleRepository _masterTitleRepository;
        public MasterTitleService(IMasterTitleRepository masterTitleRepository)
        {
            _masterTitleRepository = masterTitleRepository;
        }

        public async Task<ResultResponse> GetMasterTitlesAsync()
        {
            var titles = await _masterTitleRepository.GetMasterTitlesAsync();

            var getDropdownDto = titles.Select(s => new GetDropdownDto()
            {
                ID = s.ID,
                Name = s.Name
            });
            
            return new ResultResponse()
            {
                IsSuccess = true,
                Data = getDropdownDto
            };
        }
        
        public async Task<ResultResponse> CreateMasterTitleAsync(AddMasterDataDto addMasterDataDto)
        {
            try
            {
                var masterTitle = new MasterTitle()
                {
                    Name = addMasterDataDto.Name
                };

                var status = await _masterTitleRepository.CreateMasterTitleAsync(masterTitle);

                if (status != Constant.STATUS_SUCCESS)
                {
                    return new ResultResponse()
                    {
                        IsSuccess = false,
                        Data = status
                    };
                }

                return new ResultResponse()
                {
                    IsSuccess = true,
                    Data = status
                };
            }
            catch (Exception ex)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = ex.Message
                };
            }
        }

        public async Task<ResultResponse> UpdateMasterTitleAsync(EditMasterDataDto editMasterDataDto)
        {
            try
            {
                var masterTitle = new MasterTitle()
                {
                    Name = editMasterDataDto.Name
                };

                var status = await _masterTitleRepository.UpdateMasterTitleAsync(masterTitle);

                if (status != Constant.STATUS_SUCCESS)
                {
                    return new ResultResponse()
                    {
                        IsSuccess = false,
                        Data = status
                    };
                }

                return new ResultResponse()
                {
                    IsSuccess = true,
                    Data = status
                };
            }
            catch (Exception ex)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = ex.Message
                };
            }
        }

        public async Task<ResultResponse> DeleteMasterTitleAsync(int id)
        {
            var masterTitle = await _masterTitleRepository.GetMasterTitleByIDAsync(id);

            if (masterTitle is null)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = Constant.STATUS_DATA_NOT_FOUND
                };
            }
            
            try
            {
                var status = await _masterTitleRepository.DeleteMasterTitleAsync(masterTitle);

                if (status != Constant.STATUS_SUCCESS)
                {
                    return new ResultResponse()
                    {
                        IsSuccess = false,
                        Data = status
                    };
                }

                return new ResultResponse()
                {
                    IsSuccess = true,
                    Data = status
                };
            }
            catch (Exception ex)
            {
                return new ResultResponse()
                {
                    IsSuccess = false,
                    Data = ex.Message
                };
            }
        }
    }
}